using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Auth
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly TranAppService _tranAppMan;

        [BindProperty]
        public LoginFormModel Form { get; set; }
        public string RoleDb { get; set; }
        public string Email { get; set; }
        [TempData]
        public string SuccessMessage { get; set; }
        public LoginModel(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }

        private ClaimsPrincipal GenerateClaims()
        {
            var claims = new ClaimsIdentity(TranAppAuthenticationSchemes.Cookie);

            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Username));
            claims.AddClaim(new Claim(ClaimTypes.Email, Email));
            claims.AddClaim(new Claim(ClaimTypes.Role, RoleDb));

            return new ClaimsPrincipal(claims);
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("~/");
            }
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var loginDb = await _tranAppMan.GetLogin(Form.Username);
            if (loginDb == null)
            {
                SuccessMessage = "Username belum terdaftar.";
                return Page();
            }

            var usernameDb = loginDb.Username;
            var passwordDb = loginDb.PasswordUser;
            RoleDb = loginDb.RoleUser;
            Email = loginDb.Email;
            if (loginDb.IsActive == false)
            {
                SuccessMessage = "Account belum diaktivasi, silahkan cek email.";
                return Page();
            }

            var isUserMatched = Form.Username == usernameDb;
            var isPassMatched = _tranAppMan.Verify(Form.Password, passwordDb);

            var isCredentialsOk = isUserMatched && isPassMatched;
            if (isCredentialsOk == false)
            {
                SuccessMessage = "Invalid username or password.";
                return Page();
            }

            var claims = GenerateClaims();
            var persistence = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(15),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(TranAppAuthenticationSchemes.Cookie, claims, persistence);

            if (string.IsNullOrEmpty(returnUrl) == false)
            {
                return LocalRedirect(returnUrl);
            }

            return Redirect("~/");
        }
    }


}