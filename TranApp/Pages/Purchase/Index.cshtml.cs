using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Purchase
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class IndexModel : PageModel
    {
        private readonly TranAppService _tranAppMan;

        public List<PurchaseCartModel> Purchases { get; set; }

        //public PurchaseViewModel Pesanan { get; set; }
        
        public PurchaseModel Header { get; set; }
        public PurchaseDetailModel Cart { get; set; }
        [TempData]
        public string SuccessMessage { get; set; }

        public IndexModel(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var findLogin = await this._tranAppMan.GetLogin(userNameLogin);

            await this._tranAppMan.FetchAllById(findLogin.UserId);
            this.Purchases = this._tranAppMan.Purchases;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var findLogin = await this._tranAppMan.GetLogin(userNameLogin);
            await this._tranAppMan.FetchAllById(findLogin.UserId);
            this.Purchases = this._tranAppMan.Purchases;

            var purchaseID = this._tranAppMan.GetTime();
            var tanggalSekarang = DateTimeOffset.Now;

            Header = new PurchaseModel()
            {
                PurchaseID = purchaseID,
                UserID = findLogin.UserId,
                PurchaseDate = tanggalSekarang
            };
            await this._tranAppMan.InsertPurchaseHeader(Header);

            foreach (var item in this.Purchases)
            {
                Cart = new PurchaseDetailModel()
                {
                    ProductID = item.ProductId,
                    Quantity = item.Quantity,
                    PurchaseID = purchaseID
                };

                var result = await this._tranAppMan.InsertPurchaseDetail(Cart);
                if (result == false)
                {
                    SuccessMessage = "Failed";
                    return Page();
                }

            };

            await this._tranAppMan.SaveToDatabase();

            return Page();
        }


    }
}