using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Services;

namespace TranApp.Pages.User
{
    [AllowAnonymous]
    public class UserActivationModel : PageModel
    {
        private readonly TranAppService _tranAppMan;

        [BindProperty(SupportsGet = true)]
        public string ActivationLink { get; set; }
        [TempData]
        public string SuccessMessage { get; set; }

        public UserActivationModel(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }
        public async Task<IActionResult> OnGetAsync()
        {
            SuccessMessage = "";
            var cekActivationLink = await _tranAppMan.ActivationLink(ActivationLink);
            if (cekActivationLink == false)
            {
                return NotFound("Invalid Activation Link");
            }
            else
            {
                await _tranAppMan.ActivateUser(ActivationLink);
                SuccessMessage = "You may now login.";
                return Page();
            }
        }
    }
}
