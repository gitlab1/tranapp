using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.User
{
    [AllowAnonymous]
    public class RegisterUserModel : PageModel
    {
        private readonly TranAppService _tranAppMan;

        public RegisterUserModel(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }

        [TempData]
        public string SuccessMessage { get; set; }
        [BindProperty(SupportsGet = true)]
        public UserViewModel Form { set; get; }
        
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var checkExistUsername = await _tranAppMan.CheckExistUsername(Form.Username);
            if (checkExistUsername == false)
            {
                ModelState.AddModelError("Form.Username", "already exist");
                return Page();
            }

            var checkExistEmail = await _tranAppMan.CheckExistEmail(Form.Email);
            if (checkExistEmail == false)
            {
                ModelState.AddModelError("Form.Email", "already exist");
                return Page();
            }

            var regUser = await _tranAppMan.InsertUser(Form);
            if (regUser == false)
            {
                SuccessMessage = "Register User Failed";
                return Page();
            }
            else
            {
                SuccessMessage = "Registration Successful, please click the Activation Link that was sent to your email from Admin - No Reply.";
            }
            return Page();
            //return RedirectToPage("/auth/login");
        }
    }
}
