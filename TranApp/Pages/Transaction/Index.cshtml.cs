using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Services;

namespace TranApp.Pages.Transaction
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class IndexModel : PageModel
    {
        private readonly AppSettings _appSettings;

        public IndexModel(AppSettings appSettings)
        {
            _appSettings = appSettings;
        }
        public void OnGet()
        {
            var request = new HttpRequestMessage();
            //{
            //    RequestUri = new Uri(apiUrl),
            //    Method = HttpMethod.Post,
            //    Content = content
            //};

            request.Headers.Add("ApiKey", this._appSettings.ApiKey);
        }
    }
}
