﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TranApp.Enums;
using TranApp.Models.Commons;
using TranApp.Models.Master.Taman;
using TranApp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranApp.APIs
{
    [Authorize(Roles = UserRoles.Admin)]
    [Route("api/v1/taman")]
    public class TamanApiController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly TamanService _tamanMan;

        public TamanApiController(TamanService tamanService, AppSettings appSettings)
        {
            this._tamanMan = tamanService;
            this._appSettings = appSettings;
        }

        [HttpGet(Name = "GetAllTamanApi")]
        public async Task<ActionResult<DataGridModel<TamanViewModel>>> Get([FromQuery] TamanFilterModel filter)
        {
            //var apiKeyExist = Request.Headers.ContainsKey("ApiKey");
            //if (apiKeyExist == false)
            //{
            //    return BadRequest("API Key is not exist");
            //}

            //var headerApiKey = Request.Headers["ApiKey"].FirstOrDefault();
            //var apiKey = this._appSettings.ApiKey;
            //if (apiKey != headerApiKey)
            //{
            //    return BadRequest("Invalid API Key");
            //}

            this._tamanMan.TamanFilter = filter;

            var dataGrid = await this._tamanMan.GetAllTaman();

            return dataGrid;
        }
    }
}
