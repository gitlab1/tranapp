﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranApp.APIs
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    [Route("api/v1/tran-app")]
    [ApiController]
    public class TranAppApiController : Controller
    {
        private readonly AppSettings _appSettings;
        private readonly TranAppService _tranAppMan;

        public TranAppApiController(TranAppService tranAppService, AppSettings appSettings)
        {
            this._tranAppMan = tranAppService;
            this._appSettings = appSettings;
        }

        [HttpGet("get-all-transaction", Name = "getAllTransaction")]
        public async Task<ActionResult<List<TransactionViewModel>>> GetAllTransactionApi()
        {
            //var apiKeyExist = Request.Headers.ContainsKey("ApiKey");
            //if (apiKeyExist == false)
            //{
            //    return BadRequest("API Key is not exist");
            //}

            //var headerApiKey = Request.Headers["ApiKey"].FirstOrDefault();
            //var apiKey = this._appSettings.ApiKey;
            //if (apiKey != headerApiKey)
            //{
            //    return BadRequest("Invalid API Key");
            //}

            var data = await _tranAppMan.GetAllTran();

            return Ok(data);
        }

        [HttpGet("get-all-transaction-apikey", Name = "getAllTransactionApikey")]
        public async Task<ActionResult<List<TransactionViewModel>>> GetAllTransactionApikey()
        {
            var apiKeyExist = Request.Headers.ContainsKey("ApiKey1");
            if (apiKeyExist == false)
            {
                return BadRequest("API Key is not exist");
            }

            var headerApiKey = Request.Headers["ApiKey1"].FirstOrDefault();
            var apiKey = this._appSettings.ApiKey;
            if (apiKey != headerApiKey)
            {
                return BadRequest("Invalid API Key");
            }

            var data = await _tranAppMan.GetAllTran();

            return Ok(data);
        }
    }
}
