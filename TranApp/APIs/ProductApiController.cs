﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TranApp.Models;
using TranApp.Models.Commons;
using TranApp.Models.Master.Product;
using TranApp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranApp.APIs
{
    [AllowAnonymous]
    [Route("api/v1/product")]
    public class ProductApiController : Controller
    {
        private readonly TranAppService _tranAppMan;

        public ProductApiController(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService; ;
        }

        [HttpGet(Name = "GetProducts")]
        public async Task<ActionResult<DataGridModel<ProductViewModel>>> Get([FromQuery] ProductFilterModel filter)
        {
            this._tranAppMan.ProductFilter = filter;

            var dataGrid = await this._tranAppMan.GetProducts();

            return dataGrid;
        }
    }
}
