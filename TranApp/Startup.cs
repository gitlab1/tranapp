using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.CookiePolicy;

using TranApp.Entities;
using TranApp.Services;
using TranApp.Enums;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Options;

namespace TranApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.Lax;
                options.HttpOnly = HttpOnlyPolicy.Always;
                options.Secure = CookieSecurePolicy.SameAsRequest;
            });

            services.AddAntiforgery(Q =>
            {
                Q.Cookie.IsEssential = true;
                Q.Cookie.HttpOnly = true;
                Q.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                Q.Cookie.SameSite = SameSiteMode.Lax;
                Q.SuppressXFrameOptionsHeader = true;
            });

            services.AddRazorPages();
            services.AddDistributedMemoryCache();
            services.AddDbContextPool<TranAppDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("TranAppConnString"), strategy =>
                {
                    strategy.EnableRetryOnFailure();
                }
                );
            });

            //https://stackoverflow.com/questions/38138100/addtransient-addscoped-and-addsingleton-services-differences
            //Transient objects are always different; a new instance is provided to every controller and every service.
            //Scoped objects are the same within a request, but different across different requests.
            //Singleton objects are the same for every object and every request.

            services.Configure<AppSettings>(Configuration);
            services.AddSingleton(di => di.GetService<IOptions<AppSettings>>().Value);
            services.AddHttpClient();
            services.AddHttpContextAccessor();
            services.AddTransient<TranAppService>();
            services.AddTransient<TamanService>();

            // Harus diletakkan sebelum AddAuthentication
            //services.AddScoped<TokenServices>();
            services.AddAuthentication(TranAppAuthenticationSchemes.Cookie)
                    .AddCookie(TranAppAuthenticationSchemes.Cookie, options =>
                    {
                        options.LoginPath = "/Auth/Login";
                        options.LogoutPath = "/Auth/Logout";
                        options.AccessDeniedPath = "/Auth/AccessDenied";

                        options.Cookie.IsEssential = true;
                        options.Cookie.HttpOnly = true;
                        options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                    })
            //    .AddScheme<AuthenticationSchemeOptions, JoseAuthenticationHandler>(TrainingAuthenticationSchemes.Token, options => { })
                ;

            // agar tidak lupa dalam memberikan otorisasi setiap halaman walaupun tanpa login
            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Version = "v1", Title = "TranApp API" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "TranApp API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}
