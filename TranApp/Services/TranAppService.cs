﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.Json;
using System.Threading.Tasks;
using TranApp.Entities;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Models.Commons;
using TranApp.Models.Master.Product;
using TranApp.Models.Master.Taman;

namespace TranApp.Services
{
    public class TranAppService
    {
        private readonly AppSettings _appSettings;
        private readonly TranAppDbContext _tranAppDbContext;
        private readonly IDistributedCache _cacheMan;
        private readonly string _cacheKey = "Purchases";
        public List<PurchaseCartModel> Purchases = new List<PurchaseCartModel>();
        public List<PurchaseViewModel> ListCart = new List<PurchaseViewModel>();
        public bool IsLoaded = false;

        public ProductFilterModel ProductFilter { get; set; }

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="distributedCache"></param>
        public TranAppService(TranAppDbContext dbContext, IDistributedCache distributedCache, AppSettings appSettings)
        {
            this._tranAppDbContext = dbContext;
            this._cacheMan = distributedCache;
            this._appSettings = appSettings;
        }

        /// <summary>
        /// menampilkan semua data transaksi
        /// </summary>
        /// <returns></returns>
        public async Task<List<TransactionViewModel>> GetAllTran()
        {
            var query = from t in _tranAppDbContext.TbTransaction
                        join b in _tranAppDbContext.TbBarang
                        on t.BarangId equals b.BarangId
                        join c in _tranAppDbContext.TbCustomer
                        on t.CustomerId equals c.CustomerId
                        select new TransactionViewModel
                        {
                            TransactionId = t.TransactionId,
                            CustomerName = c.CustomerName,
                            BarangName = b.BarangName,
                            TransactionDate = t.TransactionDate
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();
            return data;
        }

        /// <summary>
        /// mendapatkan data produk berdasarkan id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<ProductViewModel> GetProductDataById(int productId)
        {
            var getProduct = await _tranAppDbContext.TbProduct
                .Where(Q => Q.ProductId == productId)
                .Select(Q => new ProductViewModel
                {
                    ProductName = Q.ProductName,
                    Price = Q.Price,
                    Stock = Q.Stock
                })
                .FirstOrDefaultAsync();

            return getProduct;
        }

        /// <summary>
        /// mendapatkan data dari DistributedCache berdasarkan id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task FetchAllById(int userId)
        {
            if (this.IsLoaded)
            {
                return;
            }

            var all = await _cacheMan.GetStringAsync(this._cacheKey);
            this.IsLoaded = true;
            if (all == null)
            {
                return;
            }

            this.Purchases = JsonSerializer.Deserialize<List<PurchaseCartModel>>(all); //1

            //this.Purchases = JsonConvert.DeserializeObject<List<PurchaseCartModel>>(all);

            this.Purchases = this.Purchases
                .Where(Q => Q.UserId == userId)
                .ToList();
        }

        /// <summary>
        /// menyimpan ke DistributedCache
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task SaveToCache(int userId)
        {
            await FetchAllById(userId);
            //var serialized = JsonConvert.SerializeObject(this.Purchases);
            var serialized = JsonSerializer.Serialize(this.Purchases); //{ProductId:1}
            await _cacheMan.SetStringAsync(this._cacheKey, serialized);
        }

        /// <summary>
        /// menyimpan data ke cart / keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task InsertCart(PurchaseViewModel model)
        {
            var getProduct = await GetProductDataById(model.ProductId);

            await FetchAllById(model.UserId);

            this.Purchases.Add(new PurchaseCartModel
            {
                UserId = model.UserId,
                ProductId = model.ProductId,
                ProductName = getProduct.ProductName,
                //PurchaseDate = DateTimeOffset.UtcNow,
                Quantity = model.Quantity
            });

            await SaveToCache(model.ProductId);
        }

        /// <summary>
        /// mendapatkan data login berdasarkan username yg login
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<LoginDbModel> GetLogin(string username)
        {
            var getData = await this._tranAppDbContext.TbUser
                .Where(Q => Q.Username == username)
                .Select(Q => new LoginDbModel
                {
                    UserId = Q.UserId,
                    Username = Q.Username,
                    PasswordUser = Q.PasswordUser,
                    RoleUser = Q.RoleUser,
                    Email = Q.Email,
                    IsActive = Q.IsActive
                }).FirstOrDefaultAsync();
            return getData;
        }

        /// <summary>
        /// Fungsi u/ verify password BCrypt
        /// </summary>
        /// <param name="notHash">password asli</param>
        /// <param name="hash">password encrypt</param>
        /// <returns></returns>
        public bool Verify(string notHash, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(notHash, hash);
        }

        /// <summary>
        /// menampilkan semua produk
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductViewModel>> GetAllProduct()
        {
            var getProduct = await _tranAppDbContext.TbProduct
                .Select(Q => new ProductViewModel
                {
                    ProductId = Q.ProductId,
                    ProductName = Q.ProductName,
                    Price = Q.Price,
                    Stock = Q.Stock
                }).ToListAsync();

            return getProduct;
        }

        /// <summary>
        /// mengecek apakah produk yg ingin dibeli sudah ada di keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CheckCartProductExist(PurchaseViewModel model)
        {
            await FetchAllById(model.UserId);

            var productExist = Purchases.Where(Q => Q.ProductId == model.ProductId).FirstOrDefault();

            if (productExist != null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// mengecek stok masih tersedia atau tidak
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CheckStock(PurchaseViewModel model)
        {
            var getProduct = await GetProductDataById(model.ProductId);

            if (model.Quantity > getProduct.Stock)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Ambil dari Cart (in memory distributed cache), lalu simpan ke ViewModel
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<PurchaseViewModel> GetPurchaseByProductId(int productId, int userId)
        {
            await FetchAllById(userId);

            var findProduct = Purchases
                .Where(Q => Q.ProductId == productId)
                .FirstOrDefault();

            if (findProduct == null)
            {
                return new PurchaseViewModel();
            }

            var result = new PurchaseViewModel()
            {
                UserId = findProduct.UserId,
                ProductId = findProduct.ProductId,
                ProductName = findProduct.ProductName,
                Quantity = findProduct.Quantity
            };

            return result;
        }

        /// <summary>
        /// Edit keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task UpdateCart(PurchaseViewModel model)
        {
            await FetchAllById(model.UserId);
            var findProduct = Purchases
                .Where(Q => Q.ProductId == model.ProductId)
                .FirstOrDefault();
            findProduct.Quantity = model.Quantity;
            await SaveToCache(model.UserId);
        }

        /// <summary>
        /// menghapus produk tertentu dari keranjang belanja
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task DeleteCart(int productId, int userId)
        {
            await FetchAllById(userId);

            var deletePurchase = Purchases
                .Where(Q => Q.ProductId == productId)
                .FirstOrDefault();

            this.Purchases.Remove(deletePurchase);

            await SaveToCache(userId);
        }

        public async Task<bool> InsertPurchaseHeader(PurchaseModel model)
        //public async Task<bool> InsertPurchase(string purchaseID, int userID, DateTimeOffset purchaseDate)
        {
            _tranAppDbContext.TbPurchase.Add(new TbPurchase
            {
                PurchaseId = model.PurchaseID,
                UserId = model.UserID,
                PurchaseDate = model.PurchaseDate
            });
            return true;
        }

        public string GetTime()
        {
            var tahun = DateTimeOffset.Now.Year.ToString();
            var bulan = DateTimeOffset.Now.Month.ToString();
            if (bulan.Length == 1)
            {
                bulan = "0" + bulan;
            }
            var tanggal = DateTimeOffset.Now.Day.ToString();
            if (tanggal.Length == 1)
            {
                tanggal = "0" + tanggal;
            }
            var jam = DateTimeOffset.Now.Hour.ToString();
            if (jam.Length == 1)
            {
                jam = "0" + jam;
            }
            var menit = DateTimeOffset.Now.Minute.ToString();
            if (menit.Length == 1)
            {
                menit = "0" + menit;
            }
            var detik = DateTimeOffset.Now.Second.ToString();
            if (detik.Length == 1)
            {
                detik = "0" + detik;
            }
            var time = tahun + bulan + tanggal + jam + menit + detik;
            return time;
        }

        public async Task<bool> InsertPurchaseDetail(PurchaseDetailModel model)
        {
            _tranAppDbContext.TbPurchaseDetail.Add(new TbPurchaseDetail
            {
                ProductId = model.ProductID,
                Quantity = model.Quantity,
                PurchaseId = model.PurchaseID
            });
            return true;
        }

        public async Task<bool> SaveToDatabase()
        {
            await this._tranAppDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// menyimpan ke 2 tabel sekaligus
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveToTransaction()
        {
            // untuk tabel header, jangan pake identity / auto increment jika ingin input ke 2 tabel / lebih
            // untuk tabel detail boleh identity dengan syarat tidak diacu oleh tabel lain
            // rekomendasi tipe data: char
            // tentukan id transaksi secara manual menggunakan algoritma
            // add ke tabel transaksi --> tanpa SaveChangesAsync()
            // add ke tabel detail transaksi --> tanpa SaveChangesAsync()
            // dst
            // SaveChangesAsync(); --> hanya 1x
            return true;
        }

        public async Task<bool> CheckExistUsername(string username)
        {
            var checkExist = await _tranAppDbContext.TbUser
                            .Where(Q => Q.Username == username)
                            .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> CheckExistEmail(string email)
        {
            var checkExist = await _tranAppDbContext.TbUser
                            .Where(Q => Q.Email == email)
                            .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hash Password using BCrypt
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public string Hash(string password)
        {
            //return BCrypt.Net.BCrypt.HashPassword(password, 12);
            return BCrypt.Net.BCrypt.HashPassword(password, 12);
        }

        public async Task<bool> ActivationLink(string activationLink)
        {
            var result = await this._tranAppDbContext.TbUser
                .Where(Q => Q.ActivationLink == activationLink)
                .FirstOrDefaultAsync();
            if (result == null)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> ActivateUser(string activationLink)
        {
            var findUser = await this._tranAppDbContext.TbUser
                .Where(Q => Q.ActivationLink == activationLink).FirstOrDefaultAsync();
            if (findUser == null)
            {
                return false;
            }
            findUser.IsActive = true;
            this._tranAppDbContext.TbUser.Update(findUser);
            await this._tranAppDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Registrasi User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> InsertUser(UserViewModel model)
        {
            var activationLink = Hash(model.Username).Replace("/", "");
            activationLink = activationLink.Replace(".", "");

            this._tranAppDbContext.TbUser.Add(new TbUser
            {
                Username = model.Username,
                PasswordUser = Hash(model.PasswordUser),
                RoleUser = UserRoles.User,
                Email = model.Email,
                IsActive = false,
                ActivationLink = activationLink
            });

            await _tranAppDbContext.SaveChangesAsync();
            await SendMail(PurposeEnum.ActivateUser, model.Email, activationLink);
            return true;
        }

        /// <summary>
        /// tidak bisa jika gmail menggunakan verifikasi 2 langkah
        /// Akses aplikasi yang kurang aman: harus AKTIF
        /// </summary>
        /// <param name="purpose"></param>
        /// <param name="destinationEmail"></param>
        /// <param name="activationLink"></param>
        /// <returns></returns>
        public async Task<bool> SendMail(string purpose, string destinationEmail, string activationLink)
        {
            var subjectMail = "";
            var bodyMail = "";
            if (purpose == PurposeEnum.ActivateUser)
            {
                subjectMail = "Activation Link";
                bodyMail = "Please click this activation link : " + _appSettings.WebAppUrl + "/User/UserActivation/" + activationLink;
            }
            else if (purpose == PurposeEnum.ForgotPassword)
            {
                subjectMail = "Forgot Password Link";
                bodyMail = "Please click this Forgot Password link : " + _appSettings.WebAppUrl + "/User/ChangeForgotPassword/" + activationLink;
            }
            var client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(_appSettings.Credentials.SmtpEmail, _appSettings.Credentials.PasswordSmtpEmail);

            var msg = new MailMessage();
            msg.To.Add(destinationEmail);
            msg.From = new MailAddress(_appSettings.Credentials.SmtpEmail, "Admin - No Reply");
            msg.Subject = subjectMail;
            msg.Body = bodyMail;
            await client.SendMailAsync(msg);
            return true;
        }

        public async Task<DataGridModel<ProductViewModel>> GetProducts()
        {
            var filter = new ProductFilterModel();

            if (this.ProductFilter != null)
            {
                filter = this.ProductFilter;
            }

            var allProduct = await _tranAppDbContext.TbProduct
                .Select(Q => new ProductViewModel
                {
                    ProductName = Q.ProductName,
                    Price = Q.Price,
                    Stock = Q.Stock
                })
                .ToListAsync();

            if (string.IsNullOrEmpty(filter.Pn) == false)
            {
                allProduct = allProduct
                .Where(Q => Q.ProductName.ToLower().Contains(filter.Pn.ToLower()))
                .ToList();
            }


            /*
             * WARNING! The impresentation of number range can be vary, depends on the requirements.
             * So, suit yourself.
             */
            if (filter.PpFrom != null && filter.PpTo != null)
            {
                allProduct = allProduct
                .Where(Q => Q.Price >= filter.PpFrom.Value && Q.Price <= filter.PpTo.Value)
                .ToList();
            }

            if (filter.IDsc == true)
            {
                switch (filter.Ob)
                {
                    case "Pp":
                        allProduct = allProduct
                            .OrderByDescending(Q => Q.Price)
                            .ToList();
                        break;
                    default:
                        allProduct = allProduct
                            .OrderByDescending(Q => Q.ProductName)
                            .ToList();
                        break;
                }
            }
            else
            {
                switch (filter.Ob)
                {
                    case "Pp":
                        allProduct = allProduct
                            .OrderBy(Q => Q.Price)
                            .ToList();
                        break;
                    default:
                        allProduct = allProduct
                            .OrderBy(Q => Q.ProductName)
                            .ToList();
                        break;
                }
            }

            var totalData = allProduct.Count;

            if (filter.P < 1)
            {
                filter.P = 1;
            }

            allProduct = allProduct
                .Skip((filter.P - 1) * filter.Ipp)
                .Take(filter.Ipp)
                .ToList();

            var dataGrid = new DataGridModel<ProductViewModel>
            {
                Data = allProduct,
                TotalData = totalData
            };

            return dataGrid;
        }


    }
}
