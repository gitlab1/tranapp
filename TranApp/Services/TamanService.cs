﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TranApp.Entities;
using TranApp.Models.Commons;
using TranApp.Models.Master.Taman;

namespace TranApp.Services
{
    public class TamanService
    {
        private readonly TranAppDbContext _tranAppDbContext;
        public TamanFilterModel TamanFilter { get; set; }

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="dbContext"></param>
        public TamanService(TranAppDbContext dbContext)
        {
            this._tranAppDbContext = dbContext;
        }

        public async Task<DataGridModel<TamanViewModel>> GetAllTaman()
        {
            var filter = new TamanFilterModel();

            if (this.TamanFilter != null)
            {
                filter = this.TamanFilter;
            }

            var allTaman = await _tranAppDbContext.TbTaman
                .Select(Q => new TamanViewModel
                {
                    TamanName = Q.TamanName,
                    TamanLocation = Q.TamanLocation,
                    TamanType = Q.TamanType
                })
                .ToListAsync();

            if (string.IsNullOrEmpty(filter.TamanName) == false)
            {
                allTaman = allTaman
                .Where(Q => Q.TamanName.ToLower().Contains(filter.TamanName.ToLower()))
                .ToList();
            }

            if (string.IsNullOrEmpty(filter.TamanLocation) == false)
            {
                allTaman = allTaman
                .Where(Q => Q.TamanLocation.ToLower().Contains(filter.TamanLocation.ToLower()))
                .ToList();
            }

            if (string.IsNullOrEmpty(filter.TamanType) == false)
            {
                allTaman = allTaman
                .Where(Q => Q.TamanType.ToLower().Contains(filter.TamanType.ToLower()))
                .ToList();
            }

            if (filter.IDsc == true)
            {
                switch (filter.Ob)
                {
                    case "TamanLocation":
                        allTaman = allTaman
                            .OrderByDescending(Q => Q.TamanLocation)
                            .ToList();
                        break;
                    case "TamanType":
                        allTaman = allTaman
                            .OrderByDescending(Q => Q.TamanType)
                            .ToList();
                        break;
                    default:
                        allTaman = allTaman
                            .OrderByDescending(Q => Q.TamanName)
                            .ToList();
                        break;
                }
            }
            else
            {
                switch (filter.Ob)
                {
                    case "TamanLocation":
                        allTaman = allTaman
                            .OrderBy(Q => Q.TamanLocation)
                            .ToList();
                        break;
                    case "TamanType":
                        allTaman = allTaman
                            .OrderBy(Q => Q.TamanType)
                            .ToList();
                        break;
                    default:
                        allTaman = allTaman
                            .OrderBy(Q => Q.TamanName)
                            .ToList();
                        break;
                }
            }

            var totalData = allTaman.Count;

            if (filter.P < 1)
            {
                filter.P = 1;
            }

            allTaman = allTaman
                .Skip((filter.P - 1) * filter.Ipp)
                .Take(filter.Ipp)
                .ToList();

            var dataGrid = new DataGridModel<TamanViewModel>
            {
                Data = allTaman,
                TotalData = totalData
            };

            return dataGrid;
        }
    }
}
