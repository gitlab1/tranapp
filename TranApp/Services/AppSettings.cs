﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Services
{
    public class AppSettings
    {
        public CredentialsSettings Credentials { get; set; }
        public string ApiKey { get; set; }
        //public string ElasticSearchUri { get; set; }
        //public string TokenSecretKey { get; set; }
        public string WebAppUrl { get; set; }
    }

    public class CredentialsSettings
    {
        public string Username { get; set; }
        public string AdminPassword { get; set; }
        public string SmtpEmail { get; set; }
        public string PasswordSmtpEmail { get; set; }
        //public string FromEmail { get; set; }
    }
}
