﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models.Master.Taman
{
    public class TamanViewModel
    {
        public int TamanId { get; set; }
        public string TamanName { get; set; }
        public string TamanLocation { get; set; }
        public string TamanType { get; set; }
    }
}
