﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TranApp.Interfaces;

namespace TranApp.Models.Master.Taman
{
    public class TamanFilterModel : BaseFilterModel
    {
        /// <summary>
        /// Taman Name filter.
        /// </summary>
        public string TamanName { get; set; }

        /// <summary>
        /// Taman Location filter.
        /// </summary>
        public string TamanLocation { get; set; }

        /// <summary>
        /// Taman Type filter.
        /// </summary>
        public string TamanType { get; set; }

    }
}
