﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class TransactionViewModel
    {
        /// <summary>
        /// keterangan...
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }

        public string BarangName { get; set; }
        public DateTimeOffset TransactionDate { get; set; }
    }
}
