﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models.Commons
{
    public class DataGridModel<T>
    {
        public List<T> Data { get; set; }

        public int TotalData { get; set; }
    }
}
