﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class PurchaseDetailModel
    {
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public string PurchaseID { get; set; }
    }
}
