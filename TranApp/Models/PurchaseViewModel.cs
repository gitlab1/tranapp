﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class PurchaseViewModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "harus diisi.")]
        [Display(Name = "Product Id")]
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        [Required(ErrorMessage = "harus diisi.")]
        [Display(Name = "Quantity")]
        public int Quantity { get; set; }
        public DateTimeOffset PurchaseDate { get; set; }
    }
}
