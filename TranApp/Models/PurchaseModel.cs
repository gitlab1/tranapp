﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    public class PurchaseModel
    {
        public string PurchaseID { get; set; }
        public int UserID { get; set; }
        public DateTimeOffset PurchaseDate { get; set; }
    }
}
