﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TranApp.Models
{
    /// <summary>
    /// In Memory Distributed Cache
    /// </summary>
    public class PurchaseCartModel
    {
        /// <summary>
        /// userid login
        /// </summary>
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        //public string PurchaseID { get; set; }
        //public DateTimeOffset PurchaseDate { get; set; }
    }
}
