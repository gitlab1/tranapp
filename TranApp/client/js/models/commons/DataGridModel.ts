﻿export interface DataGridModel{
    headers: DataGridHeaderModel[];

    data: object[];

    totalData: number;
    totalPage: number;
}

export interface DataGridHeaderModel{
    columnId: string;

    columnName: string;

    filterType?: 'String' | 'Number' | 'Date' | 'NumberRange' | 'DateRange';

    isVisible: boolean;
}

export interface EndpointDataGridModel{
    data: object[];

    totalData: number;
}

interface DataGridFilterRangeModel{
    from: string | number | Date;
    to: string | number | Date;

    type: string | number | Date;
}