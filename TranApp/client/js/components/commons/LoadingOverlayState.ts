﻿class LoadingOverlayState {
    isLoading = false;

    toggle(): void {
        this.isLoading = !this.isLoading;
    }
}

const loadingOverlayState = new LoadingOverlayState();

export default loadingOverlayState;