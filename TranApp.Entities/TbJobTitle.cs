﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbJobTitle
    {
        [Key]
        public int JobTitleId { get; set; }
        [Required]
        [StringLength(255)]
        public string JobTitleName { get; set; }
    }
}
