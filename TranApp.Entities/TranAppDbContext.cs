﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TranApp.Entities
{
    public partial class TranAppDbContext : DbContext
    {
        
        public TranAppDbContext(DbContextOptions<TranAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbBarang> TbBarang { get; set; }
        public virtual DbSet<TbCustomer> TbCustomer { get; set; }
        public virtual DbSet<TbJobTitle> TbJobTitle { get; set; }
        public virtual DbSet<TbProduct> TbProduct { get; set; }
        public virtual DbSet<TbPurchase> TbPurchase { get; set; }
        public virtual DbSet<TbPurchaseDetail> TbPurchaseDetail { get; set; }
        public virtual DbSet<TbTaman> TbTaman { get; set; }
        public virtual DbSet<TbTransaction> TbTransaction { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbBarang>(entity =>
            {
                entity.Property(e => e.BarangId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbCustomer>(entity =>
            {
                entity.Property(e => e.CustomerId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbJobTitle>(entity =>
            {
                entity.HasKey(e => e.JobTitleId)
                    .HasName("PK_JobTitle");

                entity.Property(e => e.JobTitleId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK_Product");

                entity.Property(e => e.ProductId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbPurchase>(entity =>
            {
                entity.HasKey(e => e.PurchaseId)
                    .HasName("PK_Purchase");

                entity.Property(e => e.PurchaseId).IsFixedLength();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_User");
            });

            modelBuilder.Entity<TbPurchaseDetail>(entity =>
            {
                entity.HasKey(e => e.PurchaseDetailId)
                    .HasName("PK_PurchaseDetail");

                entity.Property(e => e.PurchaseDetailId).UseIdentityAlwaysColumn();

                entity.Property(e => e.PurchaseId).IsFixedLength();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TbPurchaseDetail)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseDetail_Product");

                entity.HasOne(d => d.Purchase)
                    .WithMany(p => p.TbPurchaseDetail)
                    .HasForeignKey(d => d.PurchaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseDetail_Purchase");
            });

            modelBuilder.Entity<TbTaman>(entity =>
            {
                entity.HasKey(e => e.TamanId)
                    .HasName("PK_Taman");

                entity.Property(e => e.TamanId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbTransaction>(entity =>
            {
                entity.Property(e => e.TransactionId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.Barang)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.BarangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbTransaction_TbBarang");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbTransaction_TbCustomer");
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.ActivationLink)
                    .HasName("TbUser_ActivationLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("TbUser_Email_key")
                    .IsUnique();

                entity.HasIndex(e => e.ForgotPasswordLink)
                    .HasName("TbUser_ForgotPasswordLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("TbUser_Username_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
