﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbPurchase
    {
        public TbPurchase()
        {
            TbPurchaseDetail = new HashSet<TbPurchaseDetail>();
        }

        [Key]
        [Column("PurchaseID")]
        [StringLength(14)]
        public string PurchaseId { get; set; }
        [Column("UserID")]
        public int UserId { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset PurchaseDate { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(TbUser.TbPurchase))]
        public virtual TbUser User { get; set; }
        [InverseProperty("Purchase")]
        public virtual ICollection<TbPurchaseDetail> TbPurchaseDetail { get; set; }
    }
}
