﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbTaman
    {
        [Key]
        [Column("TamanID")]
        public int TamanId { get; set; }
        [Required]
        [StringLength(255)]
        public string TamanName { get; set; }
        [Required]
        [StringLength(255)]
        public string TamanLocation { get; set; }
        [Required]
        [StringLength(255)]
        public string TamanType { get; set; }
    }
}
