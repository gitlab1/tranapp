CREATE TABLE "TbUser"
	(
	"UserID"             INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"Username"           VARCHAR (20) NOT NULL UNIQUE,
	"PasswordUser"       VARCHAR(255) NOT NULL,
    "RoleUser"           VARCHAR(20)  NOT NULL,
	"Email"              VARCHAR(255) NOT NULL UNIQUE,
	"IsActive"           BOOLEAN      NOT NULL DEFAULT FALSE,
	"ActivationLink"     VARCHAR(255) NOT NULL UNIQUE,
	"ForgotPasswordLink" VARCHAR(255) UNIQUE,
	CONSTRAINT "PK_User" PRIMARY KEY ("UserID")
	);
	
CREATE TABLE "TbProduct"
	(
	"ProductID"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"ProductName" VARCHAR (255) NOT NULL,
	"Price"       DECIMAL NOT NULL,
	"Stock"       INT NOT NULL,
	CONSTRAINT "PK_Product" PRIMARY KEY ("ProductID")
	);	

CREATE TABLE "TbPurchase"
	(
	"PurchaseID"   CHAR(14) NOT NULL,
	"UserID"       INT NOT NULL,
	"PurchaseDate" TIMESTAMPTZ NOT NULL,
	CONSTRAINT "PK_Purchase" PRIMARY KEY ("PurchaseID"),
	CONSTRAINT "FK_Purchase_User" FOREIGN KEY ("UserID") REFERENCES "TbUser" ("UserID")
	);
	
CREATE TABLE "TbPurchaseDetail"
	(
	"PurchaseDetailID" INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"ProductID"    INT NOT NULL,
	"Quantity"     INT NOT NULL,
	"PurchaseID"   CHAR(14) NOT NULL,
	CONSTRAINT "PK_PurchaseDetail" PRIMARY KEY ("PurchaseDetailID"),
	CONSTRAINT "FK_PurchaseDetail_Product" FOREIGN KEY ("ProductID") REFERENCES "TbProduct" ("ProductID"),
	CONSTRAINT "FK_PurchaseDetail_Purchase" FOREIGN KEY ("PurchaseID") REFERENCES "TbPurchase" ("PurchaseID")	
	);	