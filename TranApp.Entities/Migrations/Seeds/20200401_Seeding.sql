INSERT INTO public."TbUser"(
	"Username", "PasswordUser", "RoleUser", "Email", "IsActive", "ActivationLink", "ForgotPasswordLink")
	VALUES ('admin', '$2y$12$mGIIaOBO70tkX50QZJ1nBem/njRnAL0BsXyU/vpyw0uFsfvUGGTx2', 'Admin', '', true, '', null);

INSERT INTO public."TbUser"(
	"Username", "PasswordUser", "RoleUser", "Email", "IsActive", "ActivationLink", "ForgotPasswordLink")
	VALUES ('sudirman', '$2y$12$vHbc2klRjIeb5n7IQZgnWuz0klvqfl2iaiUEAFj2UqZQbKcY8dITu', 'User', 'a@b.c', true, 'xyz', null);

INSERT INTO public."TbProduct"(
	"ProductName", "Price", "Stock")
	VALUES ('Ember', 5000, 10);
	
INSERT INTO public."TbProduct"(
	"ProductName", "Price", "Stock")
	VALUES ('Sapu', 12000, 7);
	
INSERT INTO public."TbProduct"(
	"ProductName", "Price", "Stock")
	VALUES ('Ciki', 9000, 15);