Create table "TbBarang"(
	"BarangId" INT  GENERATED always as IDENTITY NOT NULL,
	"BarangName" VARCHAR(255) NOT NULL,
	"BarangHarga" DECIMAL(18, 2) NOT NULL,
	"CreatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"CreatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
	"UpdatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"UpdatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
	CONSTRAINT "PK_TbBarang" PRIMARY KEY ("BarangId")
);

Create table "TbCustomer"(
	"CustomerId" INT GENERATED always as IDENTITY NOT NULL,
	"CustomerName" VARCHAR(255)NOT NULL,
	"CustomerEmail" VARCHAR(255)NOT NULL,
	"CreatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"CreatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
	"UpdatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"UpdatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
CONSTRAINT "PK_TbCustomer" PRIMARY KEY ("CustomerId")
);

Create table "TbTransaction"(
	"TransactionId" INT GENERATED always as IDENTITY NOT NULL,
	"TipeTransaksi" VARCHAR(255) NOT NULL,
	"TransactionDate" TIMESTAMPTZ,
	"CustomerId" INT NOT NULL,
	"BarangId" INT NOT NULL,
	"IsLunas" BIT NOT NULL,
	"TotalTransaction" DECIMAL(18, 2),
	"CreatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"CreatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
	"UpdatedAt" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"UpdatedBy" VARCHAR(255) NOT NULL DEFAULT 'SYSTEM',
CONSTRAINT "FK_TbTransaction_TbCustomer" FOREIGN KEY ("CustomerId") REFERENCES "TbCustomer"("CustomerId"),
CONSTRAINT "FK_TbTransaction_TbBarang" FOREIGN KEY ("BarangId") REFERENCES "TbBarang"("BarangId"),
CONSTRAINT "PK_TbTransaction" PRIMARY KEY ("TransactionId")
);