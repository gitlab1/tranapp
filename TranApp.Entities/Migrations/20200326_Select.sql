select "c"."CustomerName",
        "b"."BarangName",
        "t"."TransactionDate"
  from "TbTransaction" "t"
  join "TbBarang" "b"
    on "t"."BarangId" = "b"."BarangId"
  join "TbCustomer" "c"
    on "t"."CustomerId" = "c"."CustomerId"