﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbPurchaseDetail
    {
        [Key]
        [Column("PurchaseDetailID")]
        public int PurchaseDetailId { get; set; }
        [Column("ProductID")]
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        [Required]
        [Column("PurchaseID")]
        [StringLength(14)]
        public string PurchaseId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty(nameof(TbProduct.TbPurchaseDetail))]
        public virtual TbProduct Product { get; set; }
        [ForeignKey(nameof(PurchaseId))]
        [InverseProperty(nameof(TbPurchase.TbPurchaseDetail))]
        public virtual TbPurchase Purchase { get; set; }
    }
}
